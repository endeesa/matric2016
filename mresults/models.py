from django.db import models

# Create your models here.
class AllResults(models.Model):
    wrote = models.PositiveIntegerField(default=0)
    passed = models.PositiveIntegerField(default=0)
    year = models.CharField(max_length=8)
    name = models.CharField(max_length = 50)


