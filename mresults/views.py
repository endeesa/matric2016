from django.shortcuts import render
from .models import *
from django.db.models import *
import simplejson

import pandas as pd
import numpy as np

# Each question dicovered in EDA must have its won view

# Important info to the public - summary
# Bonus: prediction model
def index(request):
    matric_results = pd.DataFrame()
    matric_results['Yearly'] = np.asarray ( list(AllResults.objects.values_list('year', flat=True)) )
    matric_results['Wrote'] = np.asarray ( list(AllResults.objects.values_list('wrote', flat=True)) )
    matric_results['Passed'] = np.asarray ( list(AllResults.objects.values_list('passed', flat=True)) )
    matric_results['Name'] = np.asarray ( list(AllResults.objects.values_list('name', flat=True)) )

    distinct_years = list ( AllResults.objects.values_list('year', flat=True).distinct() )
    numDate = []
    for year in distinct_years:
        numDate.append(int(year))
    numDate = sorted(numDate, reverse=True)

    dates = []
    for year in numDate:
        dates.append(str(year))

    total_pass = []
    total_wrote = []
    for year in dates:
        total_wrote.append( AllResults.objects.filter(year=year).aggregate(Sum('wrote'))['wrote__sum'] )
        total_pass.append( AllResults.objects.filter(year=year).aggregate(Sum('passed') )['passed__sum'] )

    #byYear = AllResults.objects.filter(year='2014').aggregate(Sum('wrote'))
    n_average = np.array(list(total_pass))/ np.array(list(total_wrote)) * 100
    n_average = np.around(n_average,2)

    context = {'n_average':list(n_average.tolist()), 'current_pass':n_average.tolist()[0],
                'prev_pass':n_average.tolist()[1],
                'passed':simplejson.dumps(list(total_pass)),
                'wrote':simplejson.dumps(list(total_wrote)),
                'category_list':numDate}

    return render(request, 'mresults/index.html', context)

# Debugging view
def debg(request):
    matric_results = pd.DataFrame()
    matric_results['Yearly'] = np.asarray ( list(AllResults.objects.values_list('year', flat=True)) )
    matric_results['Wrote'] = np.asarray ( list(AllResults.objects.values_list('wrote', flat=True)) )
    matric_results['Passed'] = np.asarray ( list(AllResults.objects.values_list('passed', flat=True)) )
    matric_results['Name'] = np.asarray ( list(AllResults.objects.values_list('name', flat=True)) )

    distinct_years = list ( AllResults.objects.values_list('year', flat=True).distinct() )
    numDate = []
    for year in distinct_years:
        numDate.append(int(year))
    numDate = sorted(numDate)
   # distinct_years = sorted(distinct_years)

    total_pass = []
    total_wrote = []
    for year in distinct_years:
        total_wrote.append( AllResults.objects.filter(year=year).aggregate(Sum('wrote')).values() )
        total_pass.append( AllResults.objects.filter(year=year).aggregate(Sum('passed') )['passed__sum'] )

    byYear = AllResults.objects.filter(year='2014').aggregate(Sum('wrote'))

    context = {'yearly_national':byYear,'category':distinct_years,
                'passed':simplejson.dumps(list(total_pass)),
                'sizelist':len(distinct_years)}

    return render(request, 'mresults/debg.html', context)

# Calulations - Statistical calculations and trends
def trends(request):
    context = {}
    return render(request, 'mresults/chart.html', context)

# Trends / school
def each_school(request):
    context = {}
    return render(request, 'mresults/table.html', context)